<?php

namespace Jakmall\Recruitment\Calculator\Commands;

return [
    Add\AddCommand::class,
    Subtract\SubtractCommand::class,
    Multiply\MultiplyCommand::class,
    Divide\DivideCommand::class,
    Pow\PowCommand::class,
    History\HistoryListCommand::class,
    History\HistoryClearCommand::class
];
