<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Exception;

abstract class AbstractBasicCalculationCommand extends AbstractCalculationCommand
{
    /**
     * @var string
     */
    protected $PARAMETER_NAME = 'numbers';

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param CalculatorCommandParameter $parameters
     */
    protected function addAllParameters(CalculatorCommandParameter $parameters): void
    {
        try {
            $parameters->add($this->PARAMETER_NAME . '*',
                'The ' . $this->PARAMETER_NAME . ' to be ' . $this->getCommandPassiveVerb());
        } catch (Exception $e) {
        }
    }

    /**
     * @return string
     */
    protected function createCommandDescription(): string
    {
        return sprintf('%s all given %s', ucfirst($this->getCommandVerb()), ucfirst($this->PARAMETER_NAME));
    }

    /**
     * @param array $numbers
     */
    public function handle(array $numbers = []): void
    {
        $numbers = $this->getInput($this->PARAMETER_NAME);
        parent::handle($numbers);
    }

    /**
     * @return string
     */
    public function getCommandName()
    {
        return $this->getCommandVerb();
    }
}
