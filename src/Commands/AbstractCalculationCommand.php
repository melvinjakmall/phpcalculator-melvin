<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Jakmall\Recruitment\Calculator\Commands\History\HistoryList;

abstract class AbstractCalculationCommand extends AbstractCalculatorCommand
{
    /**
     * @var float|int
     */
    protected $calculationResult;

    /**
     * @var string
     */
    protected $calculationDescription;

    /**
     * @return float|int
     */
    public function getCalculationResult()
    {
        return $this->calculationResult;
    }

    /**
     * @return mixed
     */
    public function getCalculationDescription()
    {
        return $this->calculationDescription;
    }

    /**
     * @param array $numbers
     */
    public function handle(array $numbers = [])
    {
        $this->calculationResult = $this->calculateAll($numbers);
        $this->generateCalculationDescription($numbers, $this->calculationResult);
        HistoryList::addToHistory($this);
    }

    /**
     * @param array $numbers
     * @param       $result
     */
    protected function generateCalculationDescription(array $numbers, $result)
    {
        $operator = $this->getOperator();
        $glue = sprintf(' %s ', $operator);
        $numbersList = implode($glue, $numbers);
        $this->calculationDescription = $numbersList;

        $this->comment(sprintf('%s = %s', $numbersList, $result));
    }

    /**
     * @param array $numbers
     *
     * @return float|int
     */
    protected function calculateAll(array $numbers)
    {
        $number = array_pop($numbers);

        if (count($numbers) <= 0) {
            return $number;
        }
        return $this->calculate($this->calculateAll($numbers), $number);
    }

    /**
     * @param int|float $number1
     * @param int|float $number2
     *
     * @return int|float
     */
    abstract protected function calculate($number1, $number2);

    /**
     * @return string
     */
    abstract protected function getOperator();

    /**
     * @return string
     */
    abstract protected function getCommandVerb();

    /**
     * @return string
     */
    abstract protected function getCommandPassiveVerb();
}
