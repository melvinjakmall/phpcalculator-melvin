<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Illuminate\Console\Command;

abstract class AbstractCalculatorCommand extends Command
{
    /**
     * @var string
     */
    protected $signature;

    /**
     * @var string
     */
    protected $description;

    /**
     * @var CalculatorCommandParameter
     */
    protected $parameters;

    public function __construct()
    {
        $this->parameters = $this->createParameters();
        $this->signature = $this->createCommandSignature();
        $this->description = $this->createCommandDescription();

        parent::__construct();
    }

    /**
     * @return CalculatorCommandParameter
     */
    private function createParameters()
    {
        $parameters = new CalculatorCommandParameter();
        $this->addAllParameters($parameters);

        return $parameters;
    }

    /**
     * @param CalculatorCommandParameter $parameters
     */
    abstract protected function addAllParameters(CalculatorCommandParameter $parameters): void;

    /**
     * @return string
     */
    abstract protected function createCommandDescription(): string;

    /**
     * @return string
     */
    protected function createCommandSignature(): string
    {
        return $this->getCommandName() . $this->parseParameters();
    }

    /**
     * @return string
     */
    protected function parseParameters(): string
    {
        $parameterList = '';
        foreach ($this->parameters->get() as $paramName => $paramDescription) {
            $parameterList = $parameterList
                . ' {'
                . $paramName
                . ' : '
                . $paramDescription
                . '}';
        }
        return $parameterList;
    }

    /**
     * @param $argName
     *
     * @return array|string|null
     */
    protected function getInput($argName)
    {
        return $this->argument($argName);
    }

    /**
     * @return string
     */
    abstract public function getCommandName();

    /**
     * @return mixed
     */
    abstract public function handle();
}
