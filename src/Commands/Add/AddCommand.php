<?php

namespace Jakmall\Recruitment\Calculator\Commands\Add;

use Jakmall\Recruitment\Calculator\Commands\AbstractBasicCalculationCommand;

class AddCommand extends AbstractBasicCalculationCommand
{
    /**
     * @param int|float $number1
     * @param int|float $number2
     *
     * @return int|float
     */
    protected function calculate($number1, $number2)
    {
        return $number1 + $number2;
    }

    /**
     * @return string
     */
    protected function getOperator(): string
    {
        return '+';
    }

    /**
     * @return string
     */
    protected function getCommandVerb(): string
    {
        return 'add';
    }

    /**
     * @return string
     */
    protected function getCommandPassiveVerb(): string
    {
        return 'added';
    }
}
