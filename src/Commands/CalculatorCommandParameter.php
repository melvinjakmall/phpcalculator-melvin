<?php

namespace Jakmall\Recruitment\Calculator\Commands;

use Exception;

class CalculatorCommandParameter
{
    /**
     * @var array
     */
    protected $list = [];

    public function __construct()
    {
    }

    /**
     * @param string $paramName
     * @param string $paramDescription
     *
     * @throws Exception
     */
    public function add($paramName, $paramDescription = '')
    {
        if (array_key_exists($paramName, $this->list)) {
            throw new Exception($paramName . ' parameter already exist!');
        }
        $this->list[$paramName] = $paramDescription;
    }

    /**
     * @return array
     */
    public function get(): array
    {
        return $this->list;
    }

}
