<?php

namespace Jakmall\Recruitment\Calculator\Commands\History;

use Jakmall\Recruitment\Calculator\Commands\AbstractCalculatorCommand;
use Jakmall\Recruitment\Calculator\Commands\CalculatorCommandParameter;

class HistoryClearCommand extends AbstractCalculatorCommand
{
    /**
     * @return string
     */
    public function getCommandName()
    {
        return 'history:clear';
    }

    public function handle()
    {
        HistoryList::clearHistory();

        print("History cleared!\n");
    }

    protected function addAllParameters(CalculatorCommandParameter $parameters): void
    {
    }

    /**
     * @return string
     */
    protected function createCommandDescription(): string
    {
        return 'Clear saved history';
    }
}
