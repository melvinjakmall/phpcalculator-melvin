<?php

namespace Jakmall\Recruitment\Calculator\Commands\History;

class HistoryItem
{
    private static $separator = '#';
    protected $name;
    protected $description;
    protected $result;
    protected $output;
    protected $time;

    public function __construct($name, $description, $result, $time = null)
    {
        $this->name = $name;
        $this->description = $description;
        $this->result = $result;
        $this->output = $description . ' = ' . $this->result;
        $this->time = $time ?? date('Y-m-d H:i:s');
    }

    /**
     * @param $string
     *
     * @return HistoryItem
     */
    public static function decode($string)
    {
        $data = explode(static::$separator, $string);
        $historyItem = new HistoryItem($data[0], $data[1], $data[2], $data[4]);
        return $historyItem;
    }

    public function __toString()
    {
        return HistoryItem::encode($this);
    }

    /**
     * @param HistoryItem $historyItem
     *
     * @return string
     */
    public static function encode(HistoryItem $historyItem)
    {
        return $historyItem->getName() . static::$separator .
            $historyItem->getDescription() . static::$separator .
            $historyItem->getResult() . static::$separator .
            $historyItem->getOutput() . static::$separator .
            $historyItem->getTime();
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @return string
     */
    public function getOutput(): string
    {
        return $this->output;
    }

    /**
     * @return false|string
     */
    public function getTime()
    {
        return $this->time;
    }

}
