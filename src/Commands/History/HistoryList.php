<?php

namespace Jakmall\Recruitment\Calculator\Commands\History;

use Jakmall\Recruitment\Calculator\Commands\AbstractCalculationCommand;

class HistoryList
{
    /**
     * @var string
     */
    protected static $FILE_NAME = 'history.txt';

    /**
     * @var string
     */
    private static $separator = "\n";

    /**
     * @param AbstractCalculationCommand $command
     */
    public static function addToHistory(AbstractCalculationCommand $command)
    {
        $name = $command->getCommandName();
        $description = $command->getCalculationDescription();
        $result = $command->getCalculationResult();
        $newHistoryItem = new HistoryItem($name, $description, $result);

        $historyItems = static::getHistoryItemsFromFile();
        $historyItems[] = $newHistoryItem;

        static::saveHistoryItemsToFile($historyItems);
    }

    /**
     * @return  HistoryItem[]
     */
    protected static function getHistoryItemsFromFile()
    {
        $historyItemsList = [];

        static::createFileIfNotExist();

        $fileContent = file_get_contents(static::$FILE_NAME);
        if (strlen($fileContent) <= 0) {
            return $historyItemsList;
        }

        $fileContentItems = explode(static::$separator, $fileContent);

        foreach ($fileContentItems as $fileContentItemString) {
            $historyItemsList[] = HistoryItem::decode($fileContentItemString);
        }

        return $historyItemsList;
    }

    /**
     * @param               $filters
     * @param HistoryItem[] $historyList
     *
     * @return HistoryItem[]
     */
    public static function filterHistoryItems($filters, $historyList)
    {
        if ($filters == null || sizeof($filters) == 0) {
            return $historyList;
        }

        foreach ($historyList as $index => $historyItem) {
            if (!in_array($historyItem->getName(), $filters)) {
                unset($historyList[$index]);
            }
        }

        return $historyList;
    }

    protected static function createFileIfNotExist(): void
    {
        file_put_contents(static::$FILE_NAME, '', FILE_APPEND);
    }

    protected static function saveHistoryItemsToFile($historyItems): void
    {
        file_put_contents(static::$FILE_NAME, implode(static::$separator, $historyItems));
    }

    /**
     * @return HistoryItem[]
     */
    public static function getHistoryItems()
    {
        return static::getHistoryItemsFromFile();
    }

    public static function clearHistory(): void
    {
        static::clearHistoryFile();
    }

    protected static function clearHistoryFile(): void
    {
        file_put_contents(static::$FILE_NAME, '');
    }
}
