<?php

namespace Jakmall\Recruitment\Calculator\Commands\History;

use Exception;
use Jakmall\Recruitment\Calculator\Commands\AbstractCalculatorCommand;
use Jakmall\Recruitment\Calculator\Commands\CalculatorCommandParameter;

class HistoryListCommand extends AbstractCalculatorCommand
{
    /**
     * @var string
     */
    protected $PARAMETER_NAME = 'commands';

    /**
     * @param CalculatorCommandParameter $parameters
     */
    protected function addAllParameters(CalculatorCommandParameter $parameters): void
    {
        try {
            $parameters->add($this->PARAMETER_NAME . '?*',
                'Filter the history by ' . $this->PARAMETER_NAME);
        } catch (Exception $e) {
        }
    }

    /**
     * @return string
     */
    protected function createCommandDescription(): string
    {
        return 'Show calculator history';
    }

    /**
     * @return string
     */
    public function getCommandName()
    {
        return 'history:list';
    }

    public function handle()
    {
        $filters = $this->getInput($this->PARAMETER_NAME);

        $this->printHistory($filters);
    }

    /**
     * @param $filters
     *
     */
    protected function printHistory($filters): void
    {
        $historyList = HistoryList::getHistoryItems();
        $historyList = HistoryList::filterHistoryItems($filters, $historyList);

        if (sizeof($historyList) <= 0) {
            print("History is empty.\n");
            return;
        }
        $this->printHistoryTable($historyList);
    }

    /**
     * @param HistoryItem[] $historyList
     */
    protected function printHistoryTable($historyList)
    {
        $header = array('No', 'Command', 'Description', 'Result', 'Output', 'Time');
        $items = [];
        $no = 1;
        foreach ($historyList as $historyItem) {
            $items[] = array(
                $no++,
                $historyItem->getName(),
                $historyItem->getDescription(),
                $historyItem->getResult(),
                $historyItem->getOutput(),
                $historyItem->getTime()
            );
        }
        $this->table($header, $items);
    }
}
