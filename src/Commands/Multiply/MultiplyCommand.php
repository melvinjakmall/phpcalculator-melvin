<?php

namespace Jakmall\Recruitment\Calculator\Commands\Multiply;

use Jakmall\Recruitment\Calculator\Commands\AbstractBasicCalculationCommand;

class MultiplyCommand extends AbstractBasicCalculationCommand
{
    /**
     * @param int|float $number1
     * @param int|float $number2
     *
     * @return int|float
     */
    protected function calculate($number1, $number2)
    {
        return $number1 * $number2;
    }

    /**
     * @return string
     */
    protected function getOperator()
    {
        return '*';
    }

    /**
     * @return string
     */
    protected function getCommandVerb()
    {
        return 'multiply';
    }

    /**
     * @return string
     */
    protected function getCommandPassiveVerb()
    {
        return 'multiplied';
    }
}
