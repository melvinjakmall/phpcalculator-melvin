<?php

namespace Jakmall\Recruitment\Calculator\Commands\Pow;

use Exception;
use Jakmall\Recruitment\Calculator\Commands\AbstractCalculationCommand;
use Jakmall\Recruitment\Calculator\Commands\CalculatorCommandParameter;

class PowCommand extends AbstractCalculationCommand
{
    /**
     * @var string
     */
    protected $PARAMETER_NAME_BASE = 'base';

    /**
     * @var string
     */
    protected $PARAMETER_NAME_EXP = 'exp';

    /**
     * @param CalculatorCommandParameter $parameters
     */
    protected function addAllParameters(CalculatorCommandParameter $parameters): void
    {
        try {
            $parameters->add($this->PARAMETER_NAME_BASE, 'The base number');
            $parameters->add($this->PARAMETER_NAME_EXP, 'The ' . $this->getCommandVerb() . ' number');
        } catch (Exception $e) {
        }
    }

    /**
     * @return string
     */
    protected function createCommandDescription(): string
    {
        return ucfirst($this->getCommandVerb()) . ' the given number';
    }

    /**
     * @return string
     */
    protected function getOperator()
    {
        return '^';
    }

    /**
     * @return string
     */
    protected function getCommandVerb()
    {
        return 'exponent';
    }

    /**
     * @return string
     */
    protected function getCommandPassiveVerb()
    {
        return $this->getCommandVerb();
    }

    /**
     * @return string
     */
    public function getCommandName()
    {
        return 'pow';
    }

    public function handle(array $numbers = []): void
    {
        $numbers[] = $this->getInput($this->PARAMETER_NAME_BASE);
        $numbers[] = $this->getInput($this->PARAMETER_NAME_EXP);

        parent::handle($numbers);
    }

    /**
     * @param int|float $number1
     * @param int|float $number2
     *
     * @return int|float
     */
    protected function calculate($number1, $number2)
    {
        return pow($number1, $number2);
    }
}
