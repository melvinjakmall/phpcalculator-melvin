<?php

namespace Jakmall\Recruitment\Calculator\Commands\Subtract;

use Jakmall\Recruitment\Calculator\Commands\AbstractBasicCalculationCommand;

class SubtractCommand extends AbstractBasicCalculationCommand
{
    /**
     * @param int|float $number1
     * @param int|float $number2
     *
     * @return int|float
     */
    protected function calculate($number1, $number2)
    {
        return $number1 - $number2;
    }

    /**
     * @return string
     */
    protected function getOperator()
    {
        return '-';
    }

    /**
     * @return string
     */
    protected function getCommandVerb()
    {
        return 'subtract';
    }

    /**
     * @return string
     */
    protected function getCommandPassiveVerb()
    {
        return 'subtracted';
    }
}
